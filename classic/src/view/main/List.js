/**
 * This view is an example list of people.
 */
Ext.define('Example.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',
    width:700,
    y:-88,
    requires: [
        'Example.store.Personnel'
    ],

constructor: function(config) {
this.store = new Example.store.Personnel();
var me;
this.callParent(config);
    },
dockedItems:{xtype:'toolbar',
         dock:'top',
         ui:'blue-grid-toolbar-header',
         items:[{
            xtype:'label' 
            ,text:'Folder Candidates'
         ,style:'color:#ffffff'}, {
        xtype: 'textfield',
        placeholder:'Search ...',
         enableKeyEvents: true, listeners: {
            keyup: function (string) {

                console.log(this)
                var gridstore = this.up('gridpanel').getStore()

                gridstore.clearFilter();

                var dataToDelete = [];
                //iterate and set flag
                gridstore.each(function(rec, idx){
                    contains = false;

                    for (field in rec.data) {

                        if (rec.data[field].indexOf(string.getValue()) > -1) { //field contains
                            contains = true;
                        }
                    }
                    if (!contains) {
                        rec.filterMeOut = false;
                    }else {
                        rec.filterMeOut = true;
                    }
                });

                //custom filter object
                gridstore.filterBy(function(rec, id) {
                    if(rec.filterMeOut) {
                        return true;
                    }
                    else {
                        return false;
                    }
                });
            }
        }
    }
    ]},

      ui:'coregrid',

    columns: [
        { text: 'Name', ui:'coregridcolumn', dataIndex: 'name' },
        { text: 'Email', dataIndex: 'email', flex: 1 },
        { text: 'Phone', dataIndex: 'phone', flex: 1 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
