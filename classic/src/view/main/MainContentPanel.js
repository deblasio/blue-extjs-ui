/**
 * This view is an example list of people.
 */
Ext.define('Example.view.main.MainContentPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'maincontentpanel',
    
    layout:'hbox',
    style:'padding-bottom:40px;background-color:#f5f5f5;',
    bodyStyle:'background-color:#f5f5f5;',
    cls:'main-content-panel',
    requires: [
        'Example.view.main.List'
    ],
           items:[{
               flex:1
           },{
               xtype:'panel',
               layout:'absolute',
               items:{xtype:'mainlist',
               width:700,
               y:-84,
            cls:'main-content-panel'
               
           }  },{
               flex:1
           }]
           
           
});
