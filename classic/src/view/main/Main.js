/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Example.view.main.Main', {
    extend: 'Ext.container.Viewport',
    xtype: 'app-main',
    minHeight:'100%',
    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'Ext.toolbar.Toolbar',
        'Example.view.main.MainController',
        'Example.view.main.MainModel',
        'Ext.list.Tree',
        'Example.view.main.MainContentPanel',
        'Example.view.main.HeaderContentPanel'
    ],

    controller: 'main',
    viewModel: 'main',
    itemId: 'mainView',

    layout: {
            type: 'hbox'
            
            },
    items: [
        {  layout: {
            type: 'vbox',
            align: 'stretch'
            },
          flex:1,
        items:[{
            xtype:"toolbar",
        ui:'header-bar',
        width:'100%',
            items:[{
text: 'Hireserve',
ui:'header'
            },'->',{ 
                ui: 'header',
                iconCls:'fa fa-search',
                    scale:'large'
                },{ 
                ui: 'header',
                iconCls:'fa fa-history',
                    scale:'large'
                },               
                {
                    ui: 'header',
                    iconCls:'x-fa fa-navicon',
                    id: 'main-navigation-btn',
                    scale:'large',
                    handler:function(){
                        
                        Ext.getCmp("navigationTreepanel").animate({dynamic: true, to: {width: 200}})
                         }
                   }]
            },
               {xtype:'headerpanel'            
               },{
                   xtype:'maincontentpanel'
                   
               }
              ]
        }
           ,{xtype:'panel',
                width:0,
                 id: 'navigationTreepanel',
        itemId: 'navigationTreepanel',
                  animatePolicy: {
            x: true,
            width: true
        },
               items:{    animate: true,
      
        xtype: 'treelist',
        reference: 'navigationTreeList',
        itemId: 'navigationTreeList',
        id: 'navigationTreeList',
        ui: 'treenavigation',
        store: 'NavigationTree',

        expanderFirst: false,
        expanderOnly: false,
        collapsible:true,
        listeners: {

        }
            }
    }]

            });