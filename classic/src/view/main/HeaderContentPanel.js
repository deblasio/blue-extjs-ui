/**
 * This view is an example list of people.
 */
Ext.define('Example.view.main.HeaderContentPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'headerpanel',
  
    ui:'header-panel', 
    html:'<h1>test example</h1>',
    height:250
    
});
